# Getting started

Clone this repo, install flask and run the flask_app.py
<br>
you will also need to change your openai key in flask_app.py

```
pip install Flask
pip install openai==0.28
git clone https://gitlab.com/cart1416/ai-craft.git
cd ai-craft
python3 flask_app.py
```